# Super-Toston
> **Tip**: This Helm chart is awesome.

**🚩 Must be deployed from Argocdod**

Super-Toston is the almighty testing monitoring system for our cloudlets on the edge it is the beginning of our monitoring pipeline.
  
It is deployed on each and every cluster and consists of 2 types of services:

- the cronjobs which run a scheduled job every set amount of minutes for each test build and made by us, each job return its status at the end of its run and than comes 
- the 2nd service we have here - **The Edge Forwarder ( mozart )** which passes on each of those statuses to its equivalent receiver ( [Ingress](https://gitlab.com/ananon/applications/ingress)'s endpoint ) on the Management cluster.


Configuration
-----------------
The following table lists the configurable parameters of the Super-Toston chart and their default values.

| Parameter | Description  | Default |
| --------- | ----------- | ------- |
|  📗 `jobs.name`  | Name of the job |   |
|  📗 `jobs.schedule`  | CRON schedule for the job | `*/3 * * * *` |
|  📗 `jobs.imagePullSecrets`  | Which Pull-Secret is uses |  `secret` |
|  📗 `jobs.configmap.configpy`  | **Optional** - if using configpy as CM |   | 
|  📗 `jobs.configmap.data`  | **Optional** - if using raw data in a single key|    |
|  📗 `jobs.configmap.datalist`  | **Optional** - if using list of keys & values |   |
|  📗 `jobs.configmap.mount`  | **Optional** - when using a CM, where to mount it |  |
|  📗 `jobs.image.repo`  | The Job's image repository | |
|  📗 `jobs.image.name`  | The Job's image name | |
|  📗 `jobs.image.tag`  | The Job's image tag | |
|  📕 `cluster_fqdn`  | **Non-Configurable** - cluster's FQDN | |
|  📕 `cluster_name`  | **Non-Configurable** - cluster's name | |
|  📘 `namespace`  | Namespace where all the jobs run in | `toston` |
|  📘 `app_label`  | Label to help filter Toston's jobs from the other|  `toston`  |
|  📙 `endpoint`  | Endpoint in the management cluster where [Ingress](https://gitlab.com/ananon/applications/ingress) sits|   |
|  📙 `forwarder_name`  | Name of the forwarder  |  `edge-forwarder`  |
|  📙 `forwarder_image.repo`  | The forwarder's image repository |  |
|  📙 `forwarder_image.name`  | The forwarder's image name | |
|  📙 `forwarder_image.tag`  | The forwarder's image tag | |


> *📗 - job's parameters, 📕 - Parent parameters ( come from argocdod ), 📘 - Global parameters ( apply on all jobs ), 📙 - Forwarder's parameters*

Alternatively, a YAML file that specifies the values for the above parameters can be provided while installing the chart. For example,

```yaml
- name: edgecd-t
  imagePullSecrets: secreton
  configmap:
    mount: /app_argo/config
    configpy:
      argoUrl: https://argocd.apps.np.cloudlet-dev.com/
      appsApi: api/v1/clusters
      authApi: api/v1/session
      repoApi: api/v1/repositories
      reponame: cloudlet
      url: https://argocd.apps.np.cloudlet-dev.com/
  image:
    repo: cloudlet-org
    name: toston
    tag: "app_argo-latest"
```

Author Information
------------------
_Tweeto™_
